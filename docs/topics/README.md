# Lynis Ubuntu v22.04.03


## About this repository

This repository allows to apply some solutions found in the Ubuntu operating system V22.04.03, when checked with Lynis audit system.
Lynis is a tools that permit check your system with the goals of security and increase your defenses Url: [https://cisofy.com/lynis/](https://cisofy.com/lynis/)


### Requirement

If you do not have ansible installed, you can follow the installation of:

```
./bin/ansible-base-install.sh
```

This application also uses other applications such as git. You must enter the following statement in your console command line:

```
apt install git
```

### Dependencies

You must install Lynis on your computer to perform a vulnerability scan. The following bash script allows you to download the Lynis application from the repository, using git

```
./bin/lynis-install.sh
```


### Installation Instructions

example Playbook

```
./run-anisble-playbook.sh
```

License
-------

MIT

Author Information
------------------
Created in 2023 by Patricio Rojas

