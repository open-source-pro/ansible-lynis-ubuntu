#!/usr/bin/bash
DATE=`date +"%Y-%m-%d"`
TIME=`date +"%H%M%S"`
timestamp="$DATE-$TIME"

myPWD="$PWD/lynis-reports"

cd /root/lynis
#./lynis audit system

if [ ! -d $myPWD ]
then
	mkdir -p $myPWD
fi

cp /var/log/lynis-report.dat $myPWD/lynis-report-$timestamp.dat
chown pro:pro $myPWD/lynis-report-$timestamp.dat
