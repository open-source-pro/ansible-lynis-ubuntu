#!/bin/bash
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#id11

# Intalling ansible

sudo apt install python3 pip -y

# Use pip in your selected Python environment to install the Ansible package
# of your choice for the current user:

python3 -m pip install --user ansible
python3 -m pip install --user ansible-lint

# Upgrading Ansible

python3 -m pip install --upgrade --user ansible

# Adding Ansible command shell completion
python3 -m pip install --user argcomplete
activate-global-python-argcomplete

eval $(register-python-argcomplete ansible)
eval $(register-python-argcomplete ansible-config)
eval $(register-python-argcomplete ansible-console)
eval $(register-python-argcomplete ansible-doc)
eval $(register-python-argcomplete ansible-galaxy)
eval $(register-python-argcomplete ansible-inventory)
eval $(register-python-argcomplete ansible-playbook)
eval $(register-python-argcomplete ansible-pull)
eval $(register-python-argcomplete ansible-vault)


# Confirming your installation
ansible --version
python3 -m pip show ansible